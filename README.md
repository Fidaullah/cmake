# Cmake Practice and task repo

## this repo contains

**1. practice 1**:  basic practice task to get know how of cmake 

**2. practice 2**:  this follows cmake tutorial step to understand cmake

**3. task\***: these are task done from cmake card

## build and run
to run any practice or task goto that folder and
```
mkdir build
cd build/
cmake ..
make 
./<executable>
```

## practice 1
this is basic hello world program

executable=hello

## practice 2
this follow cmake documentation tutorial and is divide steps. each step overlaps previous steps.
Tutorial links: https://cmake.org/cmake/help/latest/guide/tutorial/index.html

currently on step = 12

executable=Tutorial


Options
```cmake .. -DUSE_MYMATH=(ON or OFF)```

install instructions

```cmake --install . --perfix="<INSTALL_DIR>"```
or
```cmake --install .```

testing instructions

```ctest -VV```
further reading needed

default prefix = current_dir/practice2/install/

```./Tutorial ``` gives version of program
```./Tutorial (number)``` gives sqrt of given number

to compile debug mode
```cmake -DCMAKE_BUILD_TYPE=Debug```

to compile release mode 
```cmake -DCMAKE_BUILD_TYPE=Release```

step 5 to 11 skip and will be revisted later


## task1_helloworld
this is also a basic hello world program

executable=hello_world

## task2_static_library
this task has a main file and two libraries. libraries are complied in different CMakelist.txt then thoes file are called in main CMakelist.txt

executable=main


## task3_shared_library
this is same as task2 but with shared libraries

executable=main

## task4_json 
this task is done using git submodule in this task i have add an json submodule to task4 folder and used it in my main.cpp file.
this gives me just basic understanding of how to and external project to your cmake 
understanding of json external is very basic and uses only dump command *further reading required*
added json read from file and json output to file

executable=main.out
