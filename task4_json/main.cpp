#include <iostream>
#include <fstream>
#include "nlohmann/json.hpp"

// for convenience
using json = nlohmann::json;

using namespace std;

int main()
{
    char * filedata = new char[100];
    fstream myfile("test.json");
    cout << "this string fromat test.json:\n";
    myfile.readsome(filedata, 100);
    cout << filedata << endl;
    
    myfile.seekg(0, ios::beg);
    cout << "converting to json\n";
    json file = json::parse(myfile);
    cout << file.dump()<<endl;
    json j;
    j["pi"] = 3.141;
    j["happy"] = true;
    j["name"] = "Niels";
    j["nothing"] = nullptr;
    j["answer"]["everything"] = 42;
    j["list"] = {1, 0, 2};
    j["object"] = {{"currency", "USD"}, {"value", 42.99}};
    
    fstream outputfile("out.json");
    cout << "dumping json values" << endl;

    cout << j.dump() << endl;
    outputfile << j.dump();
    return 0;
}